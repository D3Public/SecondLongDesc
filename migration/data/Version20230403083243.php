<?php

declare(strict_types=1);

namespace D3\CategoryLongtext\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\DBAL\Types\TextType;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230403083243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Check and set of potential missing columns';
    }

    /**
     * @throws SchemaException
     * @throws DBALException
     */
    public function up(Schema $schema) : void
    {
        $this->connection->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        $table = $schema->getTable('oxcategories');

        if (!$table->hasColumn('D3LONGDESC2')){
            $table->addColumn('D3LONGDESC2', (Type::getType('text')->getName()))
                ->setNotnull(true);
        }

        $table->hasColumn('D3LONGDESC2_1') ?:
            $table->addColumn('D3LONGDESC2_1', (Type::getType('text')->getName()))
                ->setNotnull(true);

        $table->hasColumn('D3LONGDESC2_2') ?:
            $table->addColumn('D3LONGDESC2_2', (Type::getType('text')->getName()))
                ->setNotnull(true);

        $table->hasColumn('D3LONGDESC2_3') ?:
            $table->addColumn('D3LONGDESC2_3', (Type::getType('text')->getName()))
                ->setNotnull(true);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
