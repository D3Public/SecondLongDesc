<?php

declare(strict_types=1);

namespace D3\CategoryLongtext\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\DBAL\Types\TextType;
use Doctrine\DBAL\Types\Type;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230802094252 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add longtext 2 field in manufacturer table';
    }

    /**
     * @throws SchemaException
     * @throws DBALException
     * @throws DatabaseErrorException
     * @throws DatabaseConnectionException
     */
    public function up(Schema $schema) : void
    {
        $this->connection->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        $table = $schema->getTable('oxmanufacturers');

        $table->hasColumn('D3LONGDESC2') ?:
            $table->addColumn('D3LONGDESC2', (Type::getType('text'))->getName())
                ->setNotnull(true);

        $table->hasColumn('D3LONGDESC2_1') ?:
            $table->addColumn('D3LONGDESC2_1', (Type::getType('text'))->getName())
                ->setNotnull(true);

        $table->hasColumn('D3LONGDESC2_2') ?:
            $table->addColumn('D3LONGDESC2_2', (Type::getType('text'))->getName())
                ->setNotnull(true);

        $table->hasColumn('D3LONGDESC2_3') ?:
            $table->addColumn('D3LONGDESC2_3', (Type::getType('text'))->getName())
                ->setNotnull(true);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
