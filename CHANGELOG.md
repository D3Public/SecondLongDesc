# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://git.d3data.de/D3Private/SecondLongDesc/compare/1.4.0...rel_1.x)

## [1.5.0](https://git.d3data.de/D3Private/SecondLongDesc/compare/1.4.0...1.5.0) - 2023-08-02
### Added
- 2 longtexts for manufacturers

## [1.4.0](https://git.d3data.de/D3Private/SecondLongDesc/compare/1.3.0...1.4.0) - 2023-04-04
### Removed
- composer.json version entry
### Added
- composer.json php requirement

## [1.3.0](https://git.d3data.de/D3Private/SecondLongDesc/compare/1.2.1...1.3.0) - 2023-04-03
### Removed
- migration additional method

## [1.2.1](https://git.d3data.de/D3Private/SecondLongDesc/compare/1.1.0...1.2.1) - 2023-04-03
### Fixed
- migration-file
  - wrong class call in migration-file 
  - file extension use-call

## [1.1.0](https://git.d3data.de/D3Private/SecondLongDesc/compare/1.0.0...1.1.0) - 2023-04-03
### Added
- changelog
### Fixed
- wrong cl-call in templates

## [1.0.0](https://git.d3data.de/D3Private/SecondLongDesc/releases/tag/1.0.0) - 2023-04-03
### Added
- implemented module