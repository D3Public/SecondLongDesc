<?php

/**
 * This Software is the property of Data Development and is protected
 * by copyright law - it is NOT Freeware.
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 * http://www.shopmodule.com
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author        D3 Data Development - Daniel Seifert <support@shopmodule.com>
 * @link          http://www.oxidmodule.com
 */

namespace D3\CategoryLongtext\Application\Controllers\Admin{

    use OxidEsales\Eshop\Application\Controller\Admin\AdminController;

    class AdminDetailsController_parent extends AdminController{}
}

namespace  D3\CategoryLongtext\Modules\Application\Controllers\Admin{

    use OxidEsales\Eshop\Application\Controller\Admin\CategoryMain as CategoryMain;

    class CategoryMain_parent extends CategoryMain{}
}