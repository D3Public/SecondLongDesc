<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

use D3\CategoryLongtext\Application\Controllers\Admin\AdminDetailsController;
use D3\CategoryLongtext\Application\Controllers\Admin\d3manufacturerlongtext1;
use D3\CategoryLongtext\Application\Controllers\Admin\d3manufacturerlongtext2;

$sMetadataVersion = '2.1';
$sModuleId = 'd3categorylongtext';
$logo = '<img src="https://logos.oxidmodule.com/d3logo_individual.svg" alt="(D3)" style="height:1em;width:1em">';

$features = <<<FEATURES
<ul>
    <li><a href="https://manager.oxidmodule.com/view.php?id=11735">#11735 - 2. Kategorielangtext</a>
    <quote>
    <strong>Hinweise:</strong>
        <b>Voraussetzungen:</b><br>
        Datenbank-Felder: <br>
        - oxcategories__d3longdesc2<br>
        - oxcategories__d3longdesc2_1<br>
        - oxcategories__d3longdesc2_2<br>
        - oxcategories__d3longdesc2_3<br>
        Diese Erweiterung bietet einen zweiten Kategorielangtext.<br>
        Angezeigt wird der zweite Langtext unter der Artikelauflistung in der Listenansicht.
    </quote>
    </li>    
    <li><a href="https://manager.oxidmodule.com/view.php?id=12128">#12128 - Herstellerlangtexte</a></li>    
</ul>
FEATURES;

$aModule = [
    'id'           => $sModuleId,
    'title'        => $logo.' weitere Kategorie- und Herstellerlangtexte',
    'description'  => $features,
    'thumbnail'    => 'picture.png',
    'version'      => '1.5.0',
    'author'       => $logo.' Data Development (Inh.: Thomas Dartsch)',
    'email'        => 'support@shopmodule.com',
    'url'          => 'https://www.oxidmodule.com/',
    'controllers' => [
        'd3clcategorylongtext'      => AdminDetailsController::class,
        'd3manufacturerlongtext1'   => d3manufacturerlongtext1::class,
        'd3manufacturerlongtext2'   => d3manufacturerlongtext2::class,
    ],
    'templates' => [
        'd3_categories_longtext.tpl'  => 'd3/categorylongtext/Application/views/admin/tpl/d3_categories_longtext.tpl',
        'd3_manufacturer_longtext.tpl'  => 'd3/categorylongtext/Application/views/admin/tpl/d3_manufacturer_longtext.tpl',
    ],
    'blocks'      => [
        [
            'template' => 'page/list/list.tpl',
            'block'    => 'page_list_listbody',
            'file'     => 'Application/views/blocks/list_second_longdescription.tpl',
        ]
    ],
    'events'       => [
        'onActivate'   => '\D3\CategoryLongtext\Setup\Events::onActivate',
        //'onDeactivate' => '\D3\CategoryLongtext\Events\Events::onDeactivate'
    ]
];
