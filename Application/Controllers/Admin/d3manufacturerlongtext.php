<?php

namespace D3\CategoryLongtext\Application\Controllers\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Model\Manufacturer;
use stdClass;

abstract class d3manufacturerlongtext extends AdminDetailsController
{
    public $sFieldName = 'd3longdesc1';
    public $_sThisTemplate = 'd3_manufacturer_longtext.tpl';

    public function render()
    {
        $tpl = parent::render();

        $this->_aViewData['edit'] = $oManufacturer = oxNew(Manufacturer::class);

        $soxId = $this->_aViewData["oxid"] = $this->getEditObjectId();
        if (isset($soxId) && $soxId != "-1") {
            // load object
            $selectedLang = Registry::getRequest()->getRequestEscapedParameter( "selectedlang");

            if (!isset($selectedLang)) {
                $selectedLang = $this->_iEditLang;
            }

            $this->_aViewData["selectedlang"] = $selectedLang;

            $oManufacturer->loadInLang($selectedLang, $soxId);

            //Disable editing for derived items
            if ($oManufacturer->isDerived()) {
                $this->_aViewData['readonly'] = true;
            }

            foreach ( Registry::getLang()->getLanguageNames() as $id => $language) {
                $oLang = new stdClass();
                $oLang->sLangDesc = $language;
                $oLang->selected = ($id == $this->_iEditLang);
                $this->_aViewData["otherlang"][$id] = clone $oLang;
            }
        }

        $this->_aViewData["editor"] = $this->_generateTextEditor("100%", 300, $oManufacturer, "oxmanufacturers__".$this->sFieldName, "list.tpl.css");

        return $tpl;
    }

    public function save()
    {
        parent::save();

        $soxId = $this->getEditObjectId();
        $aParams = Registry::getRequest()->getRequestEscapedParameter("editval");

        $manufacturer = oxNew( Manufacturer::class);
        $selectedLang = Registry::getRequest()->getRequestEscapedParameter( "selectedlang");
        $selectedLang = $selectedLang ?: 0;

        if ($soxId != "-1") {
            $manufacturer->loadInLang($selectedLang, $soxId);
        } else {
            $aParams['oxmanufacturers__oxid'] = null;
        }

        //Disable editing for derived items
        if ($manufacturer->isDerived()) {
            return;
        }

        $manufacturer->setLanguage(0);
        $manufacturer->assign($aParams);
        $manufacturer->setLanguage($selectedLang);
        $manufacturer->save();

        // set oxid if inserted
        $this->setEditObjectId($manufacturer->getId());
    }

    public function getFieldName()
    {
        return $this->sFieldName;
    }
}
