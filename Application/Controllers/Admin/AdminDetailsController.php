<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);


namespace D3\CategoryLongtext\Application\Controllers\Admin;

use Exception;
use OxidEsales\Eshop\Application\Model\Category;
use OxidEsales\Eshop\Core\Registry;
use stdClass;

class AdminDetailsController extends \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController
{
    /**
     * Loads category object data, pases it to Smarty engine and returns
     * name of template file "d3_categories_longtext.tpl".
     *
     * @return string
     */
    public function render()
    {
        parent::render();

        $this->_aViewData['edit'] = $oCategory = oxNew('oxcategory');

        $soxId = $this->_aViewData["oxid"] = $this->getEditObjectId();
        if ($soxId != "-1" && isset($soxId)) {
            // load object
            $iCatLang = Registry::getRequest()->getRequestParameter("catlang");

            if (!isset($iCatLang)) {
                $iCatLang = $this->_iEditLang;
            }

            $this->_aViewData["catlang"] = $iCatLang;

            $oCategory->loadInLang($iCatLang, $soxId);

            foreach (Registry::getLang()->getLanguageNames() as $id => $language) {
                $oLang                              = new StdClass();
                $oLang->sLangDesc                   = $language;
                $oLang->selected                    = ($id == $this->_iEditLang);
                $this->_aViewData["otherlang"][$id] = clone $oLang;
            }
        }
        $this->_aViewData["editor"] = $this->generateTextEditor(
        "100%",
        300,
        $oCategory,
        "oxcategories__d3longdesc2",
        "list.tpl.css"
        );

        return "d3_categories_longtext.tpl";
    }

    /**
     * Saves category description text to DB.
     *
     * @return void
     * @throws Exception
     */
    public function save()
    {
        parent::save();
        $soxId   = $this->getEditObjectId();
        $aParams = Registry::getRequest()->getRequestParameter('editval');

        // shopid
        $sShopID                           = Registry::getSession()->getVariable("actshop");
        $aParams['oxcategories__oxshopid'] = $sShopID;

        /** @var Category $oCategory */
        $oCategory = oxNew("oxcategory");
        $iCatLang  = Registry::getRequest()->getRequestParameter("catlang");
        $iCatLang  = $iCatLang ? $iCatLang : 0;

        if ($soxId != "-1") {
            $oCategory->loadInLang($iCatLang, $soxId);
        } else {
            $aParams['oxcategories__oxid'] = null;
        }

        $oCategory->setLanguage($iCatLang);
        $oCategory->assign($aParams);
        $oCategory->save();

        // set oxid if inserted
        $this->setEditObjectId($oCategory->getId());
    }
}
