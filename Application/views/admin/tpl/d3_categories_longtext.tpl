[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

<script type="text/javascript">
    <!--
    function loadLang(obj)
    {
        var langvar = document.getElementById("catlang");
        if (langvar != null )
            langvar.value = obj.value;
        document.myedit.submit();
    }
    //-->
</script>

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="d3clcategorylongtext">
    <input type="hidden" name="editlanguage" value="[{$editlanguage}]">
</form>

<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post" onSubmit="copyLongDesc( 'oxcategories__d3longdesc2' );" style="padding: 0px;margin: 0px;height:0px;">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="d3clcategorylongtext">
    <input type="hidden" name="fnc" value="">
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="voxid" value="[{$oxid}]">
    <input type="hidden" name="editval[oxcategories__oxid]" value="[{$oxid}]">
    <input type="hidden" name="catlang" value="[{$catlang}]">
    <input type="hidden" value="" name="editval[oxcategories__d3longdesc2]">

    [{$editor}]
    <table cellspacing="0" cellpadding="0" border="0" style="width:99%;">
        <tr>
            <td valign="top" class="edittext">
                [{if $languages}]<strong>[{oxmultilang ident="GENERAL_LANGUAGE"}]</strong>
                    <select name="catlang" class="editinput" onchange="loadLang(this)" [{$readonly}]>
                        [{foreach key=key item=item from=$languages}]
                            <option value="[{$key}]"[{if $catlang == $key}] SELECTED[{/if}]>[{$item->name}]</option>
                        [{/foreach}]
                    </select>
                [{/if}]
            </td>
        <tr>
            <td>
                <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="CATEGORY_TEXT_SAVE"}]" onClick="document.myedit.fnc.value='save'">
            </td>
        </tr>
    </table>
</form>

[{include file="bottomnaviitem.tpl"}]
[{include file="bottomitem.tpl"}]