<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

$sLangName  = "Deutsch";
$iLangNr    = 0;
$logo = '<img src="https://logos.oxidmodule.com/d3logo.svg" alt="(D3)" style="height:1em;width:1em">';

// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = [
	//Navigation
    'charset'                               => 'UTF-8',
    'd3categorylongtext'                    => $logo.' zweiter Langtext',
    'd3manufacturerlongtext1'               => $logo.' Langtext 1',
    'd3manufacturerlongtext2'               => $logo.' Langtext 2',
];
