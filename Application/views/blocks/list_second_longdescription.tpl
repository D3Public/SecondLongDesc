[{assign var="oPageNavigation" value=$oView->getPageNavigation()}]
[{if $oViewConf->getActiveClassName() == 'manufacturerlist' && 
     $actCategory && $actCategory->oxmanufacturers__d3longdesc1->value  && 
     $oPageNavigation->actPage == 1
}]
    <div class="manufacturerDescription desc1" id="mnfLongDesc1">[{oxeval var=$actCategory->oxmanufacturers__d3longdesc1}]</div>
[{/if}]

[{$smarty.block.parent}]

[{if $oViewConf->getActiveClassName() == 'alist' && 
     $actCategory && $actCategory->oxcategories__d3longdesc2->value &&
     $oPageNavigation->actPage == 1
}]
    <div class="categoryDescription" id="catLongDesc">[{oxeval var=$actCategory->oxcategories__d3longdesc2}]</div>
[{/if}]

[{if $oViewConf->getActiveClassName() == 'manufacturerlist' && 
     $actCategory && $actCategory->oxmanufacturers__d3longdesc2->value && 
     $oPageNavigation->actPage == 1
}]
    <div class="manufacturerDescription desc2" id="mnfLongDesc2">[{oxeval var=$actCategory->oxmanufacturers__d3longdesc2}]</div>
[{/if}]