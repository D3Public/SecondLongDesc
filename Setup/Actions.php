<?php

/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * https://www.d3data.de
 *
 * @copyright (C) D3 Data Development (Inh. Thomas Dartsch)
 * @author    D3 Data Development - Daniel Seifert <info@shopmodule.com>
 * @link      https://www.oxidmodule.com
 */

declare(strict_types=1);

namespace D3\CategoryLongtext\Setup;

use Exception;
use OxidEsales\DoctrineMigrationWrapper\MigrationsBuilder;
use OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration;
use OxidEsales\Eshop\Application\Controller\Admin\ShopConfiguration;
use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\Module\Module;
use OxidEsales\Eshop\Core\Module\ModuleCache;
use OxidEsales\Eshop\Core\Registry;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

use OxidEsales\EshopCommunity\Internal\Application\ContainerFactory;

class Actions
{
    const MODULEID = 'd3categorylongtext';

    /**
     * @throws Exception
     */
    public function runModuleMigrations()
    {
        /** @var MigrationsBuilder $migrationsBuilder */
        $migrationsBuilder = oxNew(MigrationsBuilder::class);
        $migrations = $migrationsBuilder->build();
        $migrations->execute('migrations:migrate', self::MODULEID);
    }

    /**
     * Regenerate views for changed tables
     * @throws Exception
     */
    public function regenerateViews()
    {
        $oDbMetaDataHandler = oxNew(DbMetaDataHandler::class);
        $oDbMetaDataHandler->updateViews();
    }

    /**
     * clear cache
     * @throws Exception
     */
    public function clearCache()
    {
        try {
            /** @var Module $oModule */
            $oModule = oxNew(Module::class);
            $oModule->load(self::MODULEID);

            /** @var ModuleCache $oModCache */
            $oModCache = oxNew(ModuleCache::class, $oModule);
            $oModCache->resetCache();

            Registry::getUtils()->resetLanguageCache();

        } catch (ContainerExceptionInterface|NotFoundExceptionInterface $e) {
            Registry::getLogger()->error($e->getMessage(), [$this]);
            Registry::getUtilsView()->addErrorToDisplay($e->getMessage());
        }
    }

    /**
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function getModuleTemplates(): array
    {
        $container = $this->getDIContainer();
        $shopConfiguration = $container->get(ShopConfiguration::class)->get();
        $moduleConfiguration = $shopConfiguration->getModuleConfiguration(self::MODULEID);

        return array_unique(array_merge(
            $this->getModuleTemplatesFromTemplates($moduleConfiguration),
            $this->getModuleTemplatesFromBlocks($moduleConfiguration)
        ));
    }

    /**
     * @param ModuleConfiguration $moduleConfiguration
     *
     * @return array
     */
    protected function getModuleTemplatesFromTemplates(ModuleConfiguration $moduleConfiguration): array
    {
        return array_map(
            function ($template) {
                return $template->getTemplateKey();
            },
            $moduleConfiguration->getTemplates()
        );
    }

    /**
     * @param ModuleConfiguration $moduleConfiguration
     *
     * @return array
     */
    protected function getModuleTemplatesFromBlocks(ModuleConfiguration $moduleConfiguration): array
    {
        return array_map(
            function ($templateBlock) {
                return basename($templateBlock->getShopTemplatePath());
            },
            $moduleConfiguration->getTemplateBlocks()
        );
    }

    /**
     * @return ContainerInterface|null
     */
    protected function getDIContainer(): ?ContainerInterface
    {
        return ContainerFactory::getInstance()->getContainer();
    }
}
